## **Golfo Dulce 2018 to 2020**

A three-year dataset of biogeochemistry and rates of anaerobic methane oxidation from the anoxic basin of Golfo Dulce in Costa Rica.


### Files
[nutrient_and_methane_concentrations.csv](https://bitbucket.org/herdisgs/golfo-dulce-methane-oxidation/src/master/nutrient_and_methane_concentrations.csv) - Concentrations of methane, nitrite, nitrate, and ammonium 2018 to 2020

[methane_oxidation_rates.csv](https://bitbucket.org/herdisgs/golfo-dulce-methane-oxidation/src/master/methane_oxidation_rates.csv) - Rates of anaerobic methane oxidation 2018 to 2020

[kinetics_of_anaerobic_methane_oxidation.csv](https://bitbucket.org/herdisgs/golfo-dulce-methane-oxidation/src/master/kinetics_of_anaerobic_methane_oxidation.csv) - Methane kinetics experiment carried out in 2019

[methylococcales.fa](https://bitbucket.org/herdisgs/golfo-dulce-methane-oxidation/src/master/methylococcales.fa) - Methylococcales sequences

[methanofastidiosa.fa](https://bitbucket.org/herdisgs/golfo-dulce-methane-oxidation/src/master/methanofastidiosa.fa) - Methanofastidiosa sequences


[raw_data](https://bitbucket.org/herdisgs/golfo-dulce-methane-oxidation/src/master/raw_data) - directory containing the raw rate / count data from 2018, 2019 and 2020

